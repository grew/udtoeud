% =============================================================================================
include "UD_to_EUD.grs"

% =============================================================================================
package FR_null_nodes_head{
  rule conj_nsubj{
    pattern{
      e1:CONJ1 -[conj]-> SUBJ2;
      e11:CONJ1 -[1=nsubj]-> SUBJ1; CONJ1[DVerbForm];
      SUBJ1[lemma <> ce|il];
      SUBJ2[Dupos=NOUN|NUM|PRON|PROPN|X];
      SUBJ2 -[orphan]-> *}
    without{CONJ1 -[obj|xcomp]-> ARG; ARG[Dupos=NOUN|NUM|PRON|PROPN|X]; CONJ1 << ARG}
    without{SUBJ2 -[case]-> *}
    commands{
      add_node CONJ2 :< SUBJ2;
      del_edge e1; add_edge CONJ1 -[E:conj]-> CONJ2;
      CONJ2.upos=CONJ1.upos; CONJ2.Dupos=CONJ1.Dupos;
      add_edge CONJ1 -[E:conj]-> CONJ2;
      CONJ2.upos=CONJ1.upos; CONJ2.Dupos=CONJ1.Dupos;
      CONJ2.DVerbForm = CONJ1.DVerbForm;
      CONJ2.textform="";CONJ2.wordform="";
       add_edge f2: CONJ2 -> SUBJ2; f2.label=e11.label; f2.enhanced=yes}
  }

%Ex: UD_French-GSD fr-ud-train_01846 - Il vole[CONJ1] en une génération de juin à août dans le nord de son aire, deux générations[OBL2], entre mars et novembre ailleurs.
  rule conj_obl-dir{
    pattern{
      e1:CONJ1 -[conj]-> OBL2;
      CONJ1[DVerbForm];
      OBL2 -[orphan]-> *}
    without{CONJ1 -[1=csubj|nsubj|obj|xcomp]-> ARG; ARG[lemma <> ce|il|le]}
    commands{
      add_node CONJ2 :< OBL2;
      del_edge e1; add_edge CONJ1 -[E:conj]-> CONJ2;
      CONJ2.upos=CONJ1.upos; CONJ2.Dupos=CONJ1.Dupos;
      CONJ2.textform="";CONJ2.wordform="";
      CONJ2.DVerbForm = CONJ1.DVerbForm;
      add_edge CONJ2 -[E:obl]-> OBL2}
  }

}

% =============================================================================================
% Creation of deep subject relations allowed by the UD guidelines.
package FR_deep_subj_ud{

% if an infinitive INF is a XCOMP direct object of a verb V (introduced with the prepostion "de"), its deep subject is the oblique argument N of V introduced with the preposition "à".
  rule xcomp_de_inf_obl_a_noun(lex from "lexicons/verbs_a_noun_de_inf.lp"){
    pattern{
      V[Dupos=VERB,lemma=lex.verb]; INF[Dupos=VERB,DVerbForm=Inf];
      V -[1=xcomp]-> INF; INF -[mark]-> DE; DE[Dupos=ADP,lemma=de];
      V -[obl:arg]-> N; N[Dupos=NOUN|NUM|PRON|PROPN|X];
      N -[case]-> PREP; PREP[Dupos=ADP,lemma="à"]}
    without{ INF -[1=nsubj|csubj]-> *}
    commands{add_edge INF -[E:nsubj]-> N}
  }

% if an infinitive INF is a XCOMP direct object of a verb V (introduced with the prepostion "de"), its deep subject is the oblique argument N of V, which is the clitic pronoun "lui" or "se".
  rule xcomp_de_inf_obl_pron(lex from "lexicons/verbs_a_noun_de_inf.lp"){
    pattern{
      V[Dupos=VERB,lemma=lex.verb]; INF[Dupos=VERB,DVerbForm=Inf];
      V -[1=xcomp]-> INF; INF -[mark]-> DE; DE[Dupos=ADP,lemma=de];
      V -[obl:arg]-> N; N[Dupos=PRON, lemma=lui|se]}
    without{ INF -[1=nsubj|csubj]-> *}
    commands{add_edge INF -[E:nsubj]-> N}
  }

}

% =============================================================================================
% Creation of all other deep subject relations.
package FR_deep_subj_all{
% Passive past participles with the attributive function.
  rule acl_part(lex from "lexicons/passivable_verbs.lp"){
    pattern{
      PART[Dupos=VERB,lemma=lex.verb,DVerbForm=Part,Tense=Past];
      N -[1=acl]-> PART}
    without{PART -[1=csubj|nsubj]-> *}
    without{PART -[mark]-> *}
    commands{add_edge PART -[E:nsubj:pass]-> N}
  }

}

% =============================================================================================
% Distribution of dependencies from a coordination to all conjuncts.
package FR_coord_dep{

% ===================================================
% Left dependents
  rule left_dep_advmod(lex from "lexicons/adverbs_degree.lp"){
    pattern{
      CONJ1 -[1=conj]-> CONJ2;
      e:CONJ1 -[advmod]-> D; D << CONJ1}
   % without{CONJ2 -[advmod|E:advmod]-> D1; D1 << CONJ2}
    without{CONJ1 -[1=aux|cop]-> AUX; AUX << D}
    without{D[lemma=lex.adverb]}
    without{CONJ2 -> D}
    commands{ add_edge CONJ2 -[E:advmod]-> D}
  }


% If the first conjunct CONJ1 of a coordination has a causative auxiliary AUX, which is not an infinitive, and if the second conjunct CONJ2 is an infinitive, CONJ1 and CONJ2 share the causative auxiliary CAUS.
  rule left_dep_aux-caus{
    pattern{
      CONJ1 -[1=conj]-> CONJ2;
      CONJ2[DVerbForm=Inf];
      CONJ1 -[aux:caus]-> AUX; AUX[VerbForm <> Inf]
    }
    without{CONJ2 -[aux:caus|E:aux:caus]-> *}
    commands{ add_edge CONJ2 -[E:aux:caus]-> AUX}
  }
  
% If the first conjunct CONJ1 of a coordination has a passive auxiliary AUX, and if the second conjunct CONJ2 is a past participle, CONJ1 and CONJ2 share the passive auxiliary AUX.
  rule left_dep_aux-pass{
    pattern{
      CONJ1 -[1=conj]-> CONJ2;
      CONJ2[VerbForm=Part,Tense=Past];
      CONJ1 -[aux:pass]-> AUX; 
    }
    without{CONJ2 -[aux:pass|E:aux:pass]-> *}
    commands{ add_edge CONJ2 -[E:aux:pass]-> AUX}
  }

% If the first conjunct CONJ1 of a coordination has a tense auxiliary AUX, and if the second conjunct CONJ2 is a past participle, CONJ1 and CONJ2 share the tense auxiliary AUX.
  rule left_dep_aux-tense{
    pattern{
      CONJ1 -[1=conj]-> CONJ2;
      CONJ2[VerbForm=Part,Tense=Past];
      CONJ1 -[aux:tense]-> AUX; 
    }
    without{CONJ2 -[aux:tense|E:aux:tense]-> *}
    commands{ add_edge CONJ2 -[E:aux:tense]-> AUX}
  }

%TODO : conditionner la règle par le fait que le verbe soit passivable.
  rule left_dep_nsubj_past-participle{
    pattern{
      CONJ1 -[1=conj]-> CONJ2;
      CONJ1 -[E:nsubj|E:nsubj:pass]-> SUBJ;
      CONJ1[VerbForm=Part,Tense=Past];
      CONJ2[VerbForm=Part,Tense=Past];
    }
    without{CONJ1 -[1=aux]-> AUX}
    without{CONJ2 -[1=aux]-> *}
    without{CONJ2 -[E:nsubj:pass]-> SUBJ}
    commands{ add_edge CONJ2 -[E:nsubj:pass]-> SUBJ}
  }
}

% =============================================================================================
strat FR_ud_to_eud{
  Seq(
    Onf(deep_features),
    Onf(FR_null_nodes_head),
    Onf(null_nodes_head),
    Onf(null_nodes_dep),
    Onf(rel_pron),
    Onf(remove_rel_ant),
    Onf(FR_deep_subj_ud),
    Onf(deep_subj_ud),
    Onf(coord_gov),
    Onf(FR_coord_dep),
    Onf(coord_dep),
    Onf(FR_deep_subj_ud), 
    Onf(deep_subj_ud),% Ex: UD_French-GSD fr-ud-test_00046 ... il se maintient et n'aura aucun mal à perdurer.
    Onf(case_mark_info),
    Onf(cleaning_rel),
    Onf(cleaning_feat)
  )
}

% =============================================================================================
strat FR_ud_to_dud{
  Seq(
    Onf(deep_features),
    Onf(FR_null_nodes_head),
    Onf(null_nodes_head),
    Onf(null_nodes_dep),
    Onf(rel_pron),
    Onf(remove_rel_ant),
    Onf(Alt(FR_deep_subj_ud,FR_deep_subj_all)),
    Onf(Alt(deep_subj_ud, deep_subj_all)),
    Onf(coord_gov),
    Onf(FR_coord_dep),
    Onf(coord_dep),
    Onf(Alt(FR_deep_subj_ud,FR_deep_subj_all)),
    Onf(Alt(deep_subj_ud, deep_subj_all)), % Ex: UD_French-GSD fr-ud-test_00046 ... il se maintient et n'aura aucun mal à perdurer.
    Onf(cleaning_rel),
    Onf(cleaning_feat)
  )
}
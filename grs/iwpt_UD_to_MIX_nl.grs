% =============================================================================================
include "iwpt_UD_to_MIX.grs"

% =============================================================================================
% Addition of the feature PronType=Rel to some adverbs.
rule NL_rel_adv_features(lex from"lexicons/nl_rel_adv.lp"){
  pattern{ADV[upos=ADV,lemma=lex.adv, ! PronType]}
  commands{ADV.PronType=Rel}
}

% =============================================================================================
% Specific properties for relative clauses.
package NL_rel_pron{

% For subject relative pronouns, the extension RELSUBJ is added to the subject dependency targeting the antecedent.
%Ex:  WR-P-P-H-0000000105\WR-P-P-H-0000000105.p.8.s.2 - Ze zocht contact met ploegleider[ANT] Zijlaard die[PRO] adviseerde[V] te gokken op de sprint.
  rule rel_subj1{
    pattern{
      ANT -[E:ref]-> PRO;
      V -[1=nsubj]-> PRO;
      e:V -[1=nsubj,!2, enhanced=yes]-> ANT}
    commands{e.2=relsubj}
  }
  rule rel_subj2{
    pattern{
      ANT -[E:ref]-> PRO;
      V -[1=nsubj]-> PRO;
      e:V -[1=nsubj,2<>relsubj, !3, enhanced=yes]-> ANT}
    commands{e.3=relsubj}
  }

% For object relative pronouns, the extension RELOBJ is added to the OBJ dependency targeting the antecedent.
%Ex: WR-P-P-H-0000000020\WR-P-P-H-0000000020.p.14.s.4 - Zij zijn de stabiele combinaties[ANT] die[PRO] ik intik[V] voor 0 of 4.
  rule rel_obj1{
    pattern{
      ANT -[E:ref]-> PRO;
      V -[1=obj]-> PRO;
      e:V -[1=obj,!2, enhanced=yes]-> ANT}
    commands{e.2=relobj}
  }
  rule rel_obj2{
    pattern{
      ANT -[E:ref]-> PRO;
      V -[1=obj]-> PRO;
      e:V -[1=obj,2<>relobj, !3, enhanced=yes]-> ANT}
    commands{e.3=relobj}
  }

}

% =============================================================================================
% Addition of the extension XSUBJ for subjects od verbs depending on control or raising verbs.
package NL_deep_subj_ud{

% if a predicate DPRED is a XCOMP dependent of another predicate PRED without a nominal object, its deep subject is the nominal subject SUBJ of PRED.
  rule xcomp_nsubj{
    pattern{
      PRED -[1=xcomp]-> DPRED;
      %DPRED[upos=ADJ|ADV|VERB];
      e:PRED -[1=csubj|nsubj]-> SUBJ}
    without{ * -[E:ref]-> SUBJ}% When a relative pronoun is subject, its antecedent is assigned the function of enhanced subject
    without{PRED -[obj]-> *}
    without{DPRED[NullSubject=Yes]}
    without{DPRED -[1=csubj|nsubj]-> *}
    commands{add_edge f:DPRED -> SUBJ; f.1=e.1; f.2=xsubj; f.enhanced=yes}
  }

% if a predicate DPRED is a XCOMP dependent of another predicate PRED with a nominal object OBJ, OBJ is the subject of DPRED.
  rule xcomp_obj{
    pattern{
      PRED -[1=xcomp]-> DPRED;
      PRED -[1=obj]-> OBJ}
    without{ * -[E:ref]-> OBJ}% When a relative pronoun is object, its antecedent is assigned the function of enhanced subject
    without{PRED -[obj:lvc]-> OBJ}
    without{DPRED[NullSubject=Yes]}
    without{DPRED -[1=csubj|nsubj]-> OBJ}
    commands{add_edge DPRED -[E:nsubj:xsubj]-> OBJ}
  }

% If an infinitive INF is a XCOMP direct object of a verb V of the lexicon "nl_verbs_dat_inf.lp", its deep subject is the IOBJ argument N of V in the dative case.
  rule xcomp_inf_iobj(lex from "lexicons/nl_verbs_dat_inf.lp"){
    pattern{
      V[Dupos=VERB,lemma=lex.verb]; INF[Dupos=VERB,DVerbForm=Inf];
      V -[1=xcomp]-> INF;
      V -[iobj]-> N}
    without{ INF -[1=nsubj|csubj]-> *}
    commands{add_edge INF -[E:nsubj:xsubj]-> N}
  }

}


%=============================================================================================
% Strategy that converts a UD Dutch annotation into a MIX annotation.
strat NL_ud_to_mix{
  Seq(
    Onf(udpipe_fix),
    %Onf( NL_rel_adv_features),% A ETUDIER PLUS EN PROFONDEUR
    Onf(deep_features),
    Onf(null_nodes_head),
    Onf(null_nodes_dep),
    Onf(rel_pron),
    Onf(NL_rel_pron),
    Onf(NL_deep_subj_ud), % Addition of the extension XSUBJ
    %Onf(Alt(deep_subj_ud, deep_subj_all)),
    Onf(deep_subj_ud),
    %Onf(coord_gov_root),
    Onf(coord_gov),
    %Onf(null_subject),
    %Onf(coord_dep_left),
    %Onf(coord_dep_left_advmod),
    %Onf(coord_dep_left_aux),
    %Onf(coord_dep_left_case),
    %Onf(coord_dep_left_cop),
    %Onf(coord_dep_left_mark),
    %Onf(coord_dep_left_obl),
    Onf(coord_dep_left_subj),
    Onf(coord_dep_right),
    Onf(NL_deep_subj_ud), % Addition of the extension XSUBJ
    %Onf(Alt(deep_subj_ud, deep_subj_all)),
    Onf(deep_subj_ud),
    Onf(rel_pron),
    Onf(case_mwe_specific),
    Onf(case_mwe),
    Onf(case_mark),
    Onf(case_info),
    %Onf(case_add),
    Onf(cc_mark),
    Onf(cc_info),
    Onf(cleaning_rel),
    Onf(copy_basic_enhanced),
    Onf(cleaning_feat) % This package follows CLEANING_REL because CLEANING_REL uses deep features.
  )
}

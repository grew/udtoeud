# Grew systems for the 2021 EUD parsing shared task

This repository contains the code used for the [IWPT 2021 shared task](https://universaldependencies.org/iwpt21/) contribution reported in the paper [Graph Rewriting for Enhanced Universal Dependencies](https://aclanthology.org/2021.iwpt-1.18/).

The goal of the Graph Rewriting Systems (in the `grs` folder) is to convert basic [UD annotations](https://universaldependencies.org/guidelines.html) into [Enhanced UD annotations](https://universaldependencies.org/u/overview/enhanced-syntax.html).

## Conversion of gold UD data from the shared task

As reported in the paper, we have manually inspected differences between the Gold evaluation data of the shared task (Gold EUD) and the output of our systems starting from Gold UD annotations.
At least for French and for English, we have identified many errors in the Gold EUD data (see tables 3 and 4 in the [paper](https://aclanthology.org/2021.iwpt-1.18.pdf)).

As discussed during the workshop, we hope that these conversions may help treebank maintainers to correct the data. 

The input data is taken from the LinDat repository [IWPT 2021 Shared Task Data and System Outputs](https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-3728).
We use only the folder `test-gold` from the repo (a copy of it is available in the current repo) in the local folder `test-gold`.

The output of our conversion is given in the `grew_from_gold_ud` folder (see below for commands).
Note that for some languages, we use a specific strategy (with suffix `_reduc`) to give an output as close as possible to the shared task data, without taking into account some enhanced layers not annotated in the given language.
For these languages, another strategy (without the suffix `_reduc`) exists in order to produce output closer to the guidelines.
Feel free to [contact us](mailto:Bruno.Guillaume@inria.fr) if you need transformations with the other strategies.

## Commands used to produce the files in `grew_from_gold_ud` folder

In order to run the conversion, the Grew software is needed (see the [installation page](https://grew.fr/usage/install/)).

For each language, Grew is used twice:
 * first, with the simple GRS `grs/MIX_to_UD.grs` which removes the enhanced layer;
 * then, with a language specific GRS `grs/iwpt_UD_to_MIX_xx.grs` (where `xx` is the language code).
 
We give below the list of command used for the conversion:

 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/ar.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_ar.grs -strat AR_ud_to_mix_reduc -o grew_from_gold_ud/ar.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/bg.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_bg.grs -strat BG_ud_to_mix_reduc -o grew_from_gold_ud/bg.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/cs.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_cs.grs -strat CS_ud_to_mix -o grew_from_gold_ud/cs.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/en.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_en.grs -strat EN_ud_to_mix -o grew_from_gold_ud/en.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/et.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_et.grs -strat ET_ud_to_mix_reduc -o grew_from_gold_ud/et.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/fi.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_fi.grs -strat FI_ud_to_mix -o grew_from_gold_ud/fi.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/fr.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_fr.grs -strat FR_ud_to_mix_reduc -o grew_from_gold_ud/fr.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/it.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_it.grs -strat IT_ud_to_mix -o grew_from_gold_ud/it.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/lt.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_lt.grs -strat LT_ud_to_mix -o grew_from_gold_ud/lt.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/lv.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_lv.grs -strat LV_ud_to_mix -o grew_from_gold_ud/lv.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/nl.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_nl.grs -strat NL_ud_to_mix -o grew_from_gold_ud/nl.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/pl.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_pl.grs -strat PL_ud_to_mix -o grew_from_gold_ud/pl.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/ru.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_ru.grs -strat RU_ud_to_mix_reduc -o grew_from_gold_ud/ru.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/sk.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_sk.grs -strat SK_ud_to_mix -o grew_from_gold_ud/sk.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/sv.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_sv.grs -strat SV_ud_to_mix -o grew_from_gold_ud/sv.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/ta.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_ta.grs -strat TA_ud_to_mix_reduc -o grew_from_gold_ud/ta.conllu`
 * `grew transform -config iwpt -grs grs/MIX_to_UD.grs -i test-gold/uk.conllu | grew transform -config iwpt -grs grs/iwpt_UD_to_MIX_uk.grs -strat UK_ud_to_mix -o grew_from_gold_ud/uk.conllu`


## Producing EUD annotations for other languages

With the Grew software installed (see the [installation page](https://grew.fr/usage/install/)), it is possible to produce EUD annotations.

For instance, the two next commands, download the `dev` file of `UD_Wolof-WTB` and add EUD annotations in the file `wo_wtb-eud-dev.conllu`.

* `wget https://raw.githubusercontent.com/UniversalDependencies/UD_Wolof-WTB/master/wo_wtb-ud-dev.conllu`
* `grew transform -config iwpt -grs grs/iwpt_UD_to_MIX.grs -strat ud_to_mix -i wo_wtb-ud-dev.conllu -o wo_wtb-eud-dev.conllu`
